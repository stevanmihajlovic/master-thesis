﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterThesis.ANN.Neural_Network
{
    /// <summary>
    /// Used to represent the whole ANN.
    /// The layers that ANN consists of should be described in derived class.
    /// In the derived class you can design the architecture of ANN by defining connectivity between neurons on different layers.
    /// </summary>
    public abstract class ArtificialNeuralNetwork
    {
        /// <summary>
        /// Gets or sets a value representing Input Layer that ANN consists of.
        /// </summary>
        protected Layer InputLayer { get; set; }

        /// <summary>
        /// Gets or sets a value representing Output Layer that ANN consists of.
        /// </summary>
        protected Layer OutputLayer { get; set; }

        /// <summary>
        /// Gets or sets a value representing Hidden Layers that ANN consists of.
        /// </summary>
        protected List<Layer> HiddenLayers { get; set; }

        /// <summary>
        /// Gets or sets a value representing all Weights that ANN consists of.
        /// </summary>
        protected List<Synapse> AllWeights { get; set; }

        /// <summary>
        /// Gets or sets value representing total number of weights.
        /// We are using it in Agent and we need it as visible Property, while it is set privatelly
        /// </summary>
        public int TotalNumberOfWeights { get; protected set; }

        /// <summary>
        /// Caluclates value of all neurons in Output Layer that ANN consists of.
        /// By doing so it will calculate value of all neurons that ANN consists of.
        /// </summary>
        /// <returns>Index of neuron with the best value in Output Layer</returns>
        public int CaluclateOutputNeuronValues()
        {
            double max = 0;
            int maxIndex = -1;
            for (int i = 0; i < OutputLayer.Neurons.Count; i++)
            {
                double currentNeuronValue = OutputLayer.Neurons[i].CalculateValue();
                if (currentNeuronValue > max)
                {
                    max = currentNeuronValue;
                    maxIndex = i;
                }
            }
            return maxIndex;
        }

        /// <summary>
        /// Updates the value of all neurons in Input Layer that ANN consists of.
        /// </summary>
        /// <param name="values"></param>
        public void UpdateInputNeuronsValues(List<double> values)
        {
            if (values.Count != InputLayer.Neurons.Count)
                throw new Exception("Total number of input neurons does not match count of passed list of values");
            for (int i = 0; i < InputLayer.Neurons.Count; i++)
            {
                InputLayer.Neurons[i].UpdateValue(values[i]);
            }
        }

        /// <summary>
        /// Updates weights for all Neurons in all layers except the first one that does not have any.
        /// </summary>
        /// <param name="weights">Weights to be used for updating.</param>
        public void UpdateAllWeights(List<double> weights)
        {
            if (weights.Count != TotalNumberOfWeights)
                throw new Exception("Total number of weights does not match count of passed list of weights");
            for (int i = 0; i < TotalNumberOfWeights; i++)
            {
                AllWeights[i].Weight = weights[i];
            }
        }

        /// <summary>
        /// Resets the value of all neurons that ANN consists of.
        /// </summary>
        public void ResetAllNeuronValues()
        {
            foreach (Neuron neuron in InputLayer.Neurons)
                neuron.ResetValue();
            foreach (Neuron neuron in OutputLayer.Neurons)
                neuron.ResetValue();
            if (HiddenLayers != null)
                foreach (Layer layer in HiddenLayers)
                    foreach (Neuron neuron in layer.Neurons)
                        neuron.ResetValue();
        }
    }
}
