﻿using MasterThesis.ANN;
using MasterThesis.ANN.Neural_Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MasterThesis
{
    static class Program
    {
        /// <summary>
        /// Gets or sets a value representing Random number generator.
        /// This should be used for all randomizations in whole solution.
        /// </summary>
        public static Random RANDOM { get; set; }

        /// <summary>
        /// Gets or sets a value representing mutation rate.
        /// This rate should be in range 0 to 1.
        /// Set it on the start of program based on user input.
        /// It can be used to define either rate on the level of the whole Individual 
        /// or rate on the level of the genes of the Individual.
        /// </summary>
        public static double MUTATION_RATE { get; set; }

        /// <summary>
        /// Gets or sets a value representing cross-over rate.
        /// This rate should be in range 0 to 1.
        /// Set it on the start of program based on user input.
        /// It defines rate on the level of the whole Individual
        /// </summary>
        public static double CROSSOVER_RATE { get; set; }

        /// <summary>
        /// Gets or sets a value representing whether the best Individual 
        /// in the Population should be retained in next generation or not.
        /// Set it on the start of program based on user input.
        /// </summary>
        public static bool ELITISM { get; set; }

        /// <summary>
        /// Gets or sets a value representing number of Individuals that will
        /// be chosen for competing in the ParentSelection.Tournament mechanism.
        /// </summary>
        public static int TOURNAMENT_SIZE { get; set; }

        /// <summary>
        /// Gets or sets a value representing scale factor for 
        /// AdultSelection.OverProduction mechanism.
        /// </summary>
        public static int OVERPRODUCTION_SCALE { get; set; }

        /// <summary>
        /// Gets or sets a value representing Parent Selection mechanism.
        /// You can create a new one by implementing new class that extends IParentSelection interface
        /// After that add a new enum entry by modifying enum representing it.
        /// Make sure to add case in switch of Population constructor.
        /// </summary>
        public static ParentSelection CHOSEN_PARENT_SELECTION { get; set; }

        /// <summary>
        /// Gets or sets a value representing Adult Selection mechanism.
        /// You can create a new one by implementing new class that extends IAdultSelection interface
        /// After that add a new enum entry by modifying enum representing it.
        /// Make sure to add case in switch of Population constructor.
        /// </summary>
        public static AdultSelection CHOSEN_ADULT_SELECTION { get; set; }

        /// <summary>
        /// Gets or sets a value representing problem that we are solving.
        /// This one represents one Individual in Population.
        /// You can create a new one by implement new class that derivses from some Genotype derived from Individual.
        /// After that add a new enum entry by modifying enum representing it.
        /// Make sure to add case in switch of Population constructor.
        /// </summary>
        public static ProblemType CHOSEN_PROBLEM_TYPE { get; set; }

        /// <summary>
        /// Gets or sets a value representing Activation Function for Neurons in Artificial Neural Network.
        /// The constructors for Layers and Neurons that Artificial Neural Network consists of 
        /// are implemented with Linear Activation Function as the default one.
        /// Even if this parameter is not used, the Artificial Neural Network will work.
        /// It can be decided in class derived from the Artificial Neural Network whether this parameter will be used.
        /// </summary>
        public static ActivationFunction CHOSEN_ACTIVATION_FUNCTION { get; set; }
        
        /// <summary>
        /// Gets or sets a value representing ANN that will be used in entire project.
        /// Project only has one ANN that should be passed as reference to constructor of all Individuals.
        /// </summary>
        public static ArtificialNeuralNetwork ANN { get; set; }

        /// <summary>
        /// Gets or sets a value representing max steps for Agent.
        /// Only used for Agent Problem type, not crucial for GA or ANN.
        /// </summary>
        public static int MAX_STEPS { get; set; }

        /// <summary>
        /// Gets or sets a value representing award factor for Agent to use for Calculating Fitness.
        /// Only used for Agent Problem type, not crucial for GA or ANN.
        /// </summary>
        public static double AWARD_FACTOR { get; set; }

        /// <summary>
        /// Gets or sets a value representing penalty factor for Agent to use for Calculating Fitness.
        /// Only used for Agent Problem type, not crucial for GA or ANN.
        /// </summary>
        public static double PENALTY_FACTOR { get; set; }

        /// <summary>
        /// Gets or sets a value representing map for Agent to move on.
        /// Only used for Agent Problem type, not crucial for GA or ANN.
        /// </summary>
        public static Grid MAP { get; set; }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ANN_Evolved_Agent());
        }
    }
}
