﻿using MasterThesis.Genetic_Algorithm.Individuals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterThesis.Genetic_Algorithm.Parent_Selection
{
    public class FitnessProportionate : IParentSelection
    {
        /// <summary>
        /// Imagine population as pie chart where each individual represents piece of it.
        /// bigger fitess => bigger piece of pie chart => bigger chance to be selected!
        /// </summary>
        /// <param name="population"></param>
        /// <returns></returns>
        public Individual SelectParent(Population population)
        {
            double randomNumber = Program.RANDOM.NextDouble() * population.TotalFitness;
            int index;
            for (index = 0; index < population.PopulationSize && randomNumber > 0; ++index)
            {
                randomNumber -= population.Individuals.ElementAt(index).Fitness;
            }
            return population.Individuals.ElementAt(index - 1);
        }
    }
}
