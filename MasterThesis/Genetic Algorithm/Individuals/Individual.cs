﻿using MasterThesis.ANN.Neural_Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterThesis.Genetic_Algorithm.Individuals
{
    /// <summary>
    /// This class should be used to represent Individual in population.
    /// What genes actually are should be described in Genotype class.
    /// </summary>
    public abstract class Individual
    {
        /// <summary>
        /// Used to represent ANN that will be used in CalculateFitness.
        /// It could have been a parameter for CalculateFitness, but I think this is better.
        /// </summary>
        protected ArtificialNeuralNetwork ANN { get; set; }

        /// <summary>
        /// Gets or sets value representing the length of gene material.
        /// Use this property to define gene Property that represents gene material in the Genotype and set it there.
        /// </summary>
        protected int GeneLength { get; set; }   // Protected so derived Genotypes can set it

        /// <summary>
        /// Gets or sets value representing fitness of the individual.
        /// Set this property in the implementation of CalculateFitness method in the Phenotype.
        /// </summary>
        public double Fitness { get; set; } // Leave it public as some Selection mechanisms can change it

        /// <summary>
        /// Method that describes Genetic Operator Mutation.
        /// This method should be implemented in the Genotype.
        /// It does not generate new Individual but changes the one that called the method.
        /// Decide how to use MUTATION_RATE in the implementation of this method.
        /// </summary>
        public abstract void Mutation();

        /// <summary>
        /// Method that describes Genetic Operator Mutation.
        /// This method should be implemented in the Genotype.
        /// </summary>
        /// <param name="secondParent">Second provided parent.</param>
        /// <returns>
        /// It generates new Individual(s) or simply creates copies of the Individual 
        /// that called the method and second provided parent depending on CROSSOVER_RATE.
        /// Decide here whether you will return both of the new created Individuals or only one.
        /// </returns>
        public abstract Individual[] CrossOver(Individual secondParent);

        /// <summary>
        /// Calculates the fitness of the Individual and sets the Fitness property.
        /// This method should be implemented in the Phenotype.
        /// </summary>
        public abstract void CalculateFitness();

        /// <summary>
        /// This method is crucial for CrossOver as we do no want to use the same Individuals that called CrossOver.
        /// If we use the same ones, Mutation could then change them in the current generation of current population 
        /// before creating the complete offspring population and we do not want that to happen
        /// This method should be implemented in the Phenotype.
        /// </summary>
        /// <returns>An identical copy of the Individual</returns>
        public abstract Individual MakeACopy();

        /// <summary>
        /// This "Property" should be used to connect ANN and GA. It represents number of Neurons in Input Layer of ANN.
        /// Define value of this property in Phenotype of the problem that you are currently solving.
        /// </summary>
        /// <returns></returns>
        public abstract int NumberOfInputNeurons();

        /// <summary>
        /// This "Property" should be used to connect ANN and GA. It represents number of Neurons in Output Layer of ANN.
        /// Define value of this property in Phenotype of the problem that you are currently solving.
        /// </summary>
        /// <returns></returns>
        public abstract int NumberOfOutputNeurons();
    }
}
