﻿using MasterThesis.ANN.Neural_Network;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterThesis.Genetic_Algorithm.Individuals
{
    public abstract class BitGenotype : Individual
    {
        /// <summary>
        /// Gets or sets value representing the gene material.
        /// </summary>
        public BitArray Genes { get; set; }

        /// <summary>
        /// Initializes random Individual with specified gene length and boundaries of gene values if provided.
        /// </summary>
        /// <param name="geneLength">Specified gene length.</param>
        public BitGenotype(int geneLength)
        {
            GeneLength = geneLength;
            Genes = new BitArray(GeneLength);
            for (int i = 0; i < GeneLength; i++)
                Genes[i] = (Program.RANDOM.NextDouble() <= 0.5);
        }

        /// <summary>
        /// Initializes random Individual with specified ANN to be used for calculating fitness
        /// and specified length of bits that will represent one wieght in ANN.
        /// It will Initialize Individual with total gene length of ANN.TotalNumberOfWeights * weightBitLength.
        /// </summary>
        /// <param name="ann">Specified ANN.</param>
        /// <param name="weightBitLength">Specified length of bits.</param>
        public BitGenotype(ArtificialNeuralNetwork ann, int weightBitLength)
            : this (ann.TotalNumberOfWeights * weightBitLength)
        {
            ANN = ann;
        }

        /// <summary>
        /// Mutates Individual by inverting values for certain genes depending on MUTATION_RATE.
        /// We use MUTATION_RATE as rate on the level of the genes of the Individual. 
        /// </summary>
        public override void Mutation()
        {
            for (int i = 0; i < GeneLength; i++)
                if (Program.RANDOM.NextDouble() < Program.MUTATION_RATE)
                    Genes[i] = !Genes[i];
        }

        /// <summary>
        /// Combines genes of this Individual with genes another second provided parent with one cross point.
        /// </summary>
        /// <param name="secondParent">Second provided parent.</param>
        /// <returns>
        /// It generates new Individual(s) or simply creates copies of the Individual 
        /// that called the method and second provided parent depending on CROSSOVER_RATE.
        /// Returns both of the new created Individuals.
        /// </returns>
        public override Individual[] CrossOver(Individual secondParent)
        {
            Individual[] newIndiv = new BitGenotype[2];
            newIndiv[0] = this.MakeACopy();
            newIndiv[1] = secondParent.MakeACopy();

            if (Program.RANDOM.NextDouble() < Program.CROSSOVER_RATE)
            {
                int crossPoint = Program.RANDOM.Next(GeneLength);
                int i;
                for (i = 0; i < crossPoint; i++)
                {
                    ((BitGenotype)newIndiv[0]).Genes[i] = ((BitGenotype)secondParent).Genes[i];
                    ((BitGenotype)newIndiv[1]).Genes[i] = Genes[i];
                }
                for (; i < GeneLength; i++)
                {
                    ((BitGenotype)newIndiv[0]).Genes[i] = Genes[i];
                    ((BitGenotype)newIndiv[1]).Genes[i] = ((BitGenotype)secondParent).Genes[i];
                }
            }

            return newIndiv;
        }
        
        /// <summary>
        /// Represents Individual as array of 0's and 1's.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string a = "";
            for (int i = 0; i < GeneLength; i++)
                if (Genes[i])
                    a += 1;
                else
                    a += 0;
            return a;
        }
    }
}
