﻿using MasterThesis.ANN.Neural_Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterThesis.Genetic_Algorithm.Individuals
{
    public abstract class DoubleGenotype : Individual
    {
        /// <summary>
        /// Gets or sets value representing the gene material.
        /// </summary>
        public List<double> Genes { get; set; }

        /// <summary>
        /// Initializes random Individual with specified gene length.
        /// </summary>
        /// <param name="geneLength">Specified gene length.</param>
        public DoubleGenotype(int geneLength)
        {
            GeneLength = geneLength;
            Genes = new List<double>(GeneLength);
            for (int i = 0; i < GeneLength; i++)
                Genes.Add(Program.RANDOM.NextDouble());
        }

        /// <summary>
        /// Initializes random Individual with specified ANN to be used for calculating fitness.
        /// It will Initialize Individual with total gene length of ANN.TotalNumberOfWeights.
        /// </summary>
        /// <param name="ann">Specified ANN.</param>
        public DoubleGenotype(ArtificialNeuralNetwork ann)
            : this(ann.TotalNumberOfWeights)
        {
            ANN = ann;
        }

        /// <summary>
        /// Mutates Individual by assigning new random values for certain genes depending on MUTATION_RATE.
        /// We use MUTATION_RATE as rate on the level of the genes of the Individual. 
        /// </summary>
        public override void Mutation()
        {
            for (int i = 0; i < GeneLength; i++)
                if (Program.RANDOM.NextDouble() < Program.MUTATION_RATE)
                    Genes[i] = Program.RANDOM.NextDouble();
        }

        /// <summary>
        /// Combines genes of this Individual with genes another second provided parent with one cross point.
        /// </summary>
        /// <param name="secondParent">Second provided parent.</param>
        /// <returns>
        /// It generates new Individual(s) or simply creates copies of the Individual 
        /// that called the method and second provided parent depending on CROSSOVER_RATE.
        /// Returns both of the new created Individuals.
        /// </returns>
        public override Individual[] CrossOver(Individual secondParent)
        {
            Individual[] newIndiv = new DoubleGenotype[2];
            newIndiv[0] = this.MakeACopy();
            newIndiv[1] = secondParent.MakeACopy();

            if (Program.RANDOM.NextDouble() < Program.CROSSOVER_RATE)
            {
                int crossPoint = Program.RANDOM.Next(GeneLength);
                int i;
                for (i = 0; i < crossPoint; i++)
                {
                    ((DoubleGenotype)newIndiv[0]).Genes[i] = ((DoubleGenotype)secondParent).Genes[i];
                    ((DoubleGenotype)newIndiv[1]).Genes[i] = Genes[i];
                }
                for (; i < GeneLength; i++)
                {
                    ((DoubleGenotype)newIndiv[0]).Genes[i] = Genes[i];
                    ((DoubleGenotype)newIndiv[1]).Genes[i] = ((DoubleGenotype)secondParent).Genes[i];
                }
            }

            return newIndiv;
        }

        /// <summary>
        /// Represents Individual as array of doubles with two decimal places separated with ;.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string a = "";
            for (int i = 0; i < GeneLength; i++)
                a += Math.Round(Genes[i], 2) + "; ";
            return a;
        }
    }
}
