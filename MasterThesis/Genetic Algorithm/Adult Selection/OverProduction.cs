﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterThesis.Genetic_Algorithm.Adult_Selection
{
    public class OverProduction : IAdultSelection
    {
        public Population CreateOffspringPopulation(Population parents)
        {
            Population offspring = new Population(Program.OVERPRODUCTION_SCALE * parents.PopulationSize);
            if (Program.ELITISM)
            {
                offspring.Individuals.Add(parents.MaxFitIndividual.MakeACopy());
            }
            while (offspring.Individuals.Count < offspring.PopulationSize)
            {
                var firstParent = parents.SelectParent();
                var secondParent = parents.SelectParent();
                var newIndividuals = firstParent.CrossOver(secondParent);
                foreach (var newIndividual in newIndividuals)
                {
                    newIndividual.Mutation();
                    offspring.Individuals.Add(newIndividual);
                    if (offspring.Individuals.Count == offspring.PopulationSize)
                        break;
                }
            }
            offspring.UpdateFitnessAndPopulationData();
            return offspring;
        }

        public void SelectAdults(ref Population parents, Population offspring)
        {
            var bestHalfOfOffspring = offspring.Individuals.OrderByDescending(item => item.Fitness).ToList();
            bestHalfOfOffspring.RemoveRange(offspring.PopulationSize, offspring.Individuals.Count - offspring.PopulationSize);
            parents.Individuals.Clear();
            foreach (var individual in bestHalfOfOffspring)
            {
                parents.Individuals.Add(individual);
            }
            parents.UpdatePopulationData();
        }
    }
}
