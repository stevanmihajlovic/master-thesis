﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterThesis
{
    public enum AdultSelection
    {
        GenerationMixing,
        OverProduction,
        FullGenerationReplacement        
    }

    public enum ParentSelection
    {
        //FitnessProportionate,
        //Rank,
        Tournament,
        SigmaScaling
    }

    public enum ActivationFunction
    {
        Linear,
        Sigmoid,
        HyperbolicTangent
    }

    public enum ProblemType
    {
        ANNBitAgent,
        ANNDoubleAgent
    }

    public enum TileContent
    {
        Nothing,
        Food,
        Poison
    }

    public enum Direction
    {
        Up,
        Right,
        Down,
        Left
    }
}
